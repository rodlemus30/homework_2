# Homework 2

Encode&Decode CLI, made with Node.JS and TypeScript

## Installation

```bash
npm install
```

## Usage

```typescript
ts-node src/index.ts -d
ts-node src/index.ts -e
```

## License

[ISC](https://opensource.org/licenses/ISC)
