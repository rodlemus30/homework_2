import yargs from 'yargs';
import util from 'util';
import fs from 'fs';
import {
  makeStringMetaData, encodeMetaData, decodeString, testEncodeData,
} from './encodeAndDecode';

export const args = yargs.help().argv;
const filePath: string = args._[0]; // File Given
const readFile = util.promisify(fs.readFile);
const exitMesage = 'Completed!';
const badMessage = '[no-flags error] use a flag -e or -d according to your needs';

const fileExist = async (file:string):Promise<{dataCleared:string, filePath:string}> => {
  try {
    const content = await readFile(file, 'utf-8');
    const dataCleared = content.replace(/[^a-zA-Z]+/g, '');
    return { dataCleared, filePath };
  } catch (error) {
    throw new Error('File not exist...');
  }
};

const fileExistDecoded = async (file:string):Promise<{content:string, filePath:string}> => {
  try {
    const content = await readFile(file, 'utf-8');

    return { content, filePath };
  } catch (error) {
    throw new Error('File not exist...');
  }
};
export const validflags = async (flags:any):Promise<string> => {
  const { e, d } = flags;
  if (e && d) { // the two flags at the same time
    return 'Cannot use two procces at the same time...';
  }

  // encode
  if (e) {
    const { dataCleared, filePath } = await fileExist(flags._[0]);
    const metadataString = makeStringMetaData(dataCleared);
    const dataEncoded = encodeMetaData(metadataString);
    fs.writeFileSync(filePath, dataEncoded);

    return exitMesage;
  }
  // Decode
  if (d) {
    const { content, filePath } = await fileExistDecoded(flags._[0]);
    if (testEncodeData(content)) return 'Wrong Patron';
    const dataDecoded = decodeString(content);

    fs.writeFileSync(filePath, dataDecoded);
    return exitMesage;
  }
  return badMessage;
};
