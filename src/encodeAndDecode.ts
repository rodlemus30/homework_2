interface ImetaData {
   lon:number;
   val:string;
}
export const makeStringMetaData = (data:string): ImetaData[] => {
  const dataSplited = data.split('');
  let indexJump = 0;
  const dataCleared: ImetaData[] = [];
  let flag = true;

  if (dataSplited.length === 0) throw new Error('Empty file');
  dataSplited.forEach((elementValue, index) => {
    if (data[index] !== data[index + 1]) {
      // when the first item and second items are differents
      if (indexJump + 1 === 1 && index + 1 === 1) {
        flag = false; // if the first group is at the beginning flag change
        const lon = dataSplited.slice(0, 1).length;
        const val = dataSplited.slice(0, 1)[0];
        dataCleared.push({ lon, val });
      }

      if (indexJump >= 0 && index >= 1) {
        const lon = dataSplited.slice(indexJump + 1, index + 1).length;
        const val = dataSplited.slice(indexJump + 1, index + 1)[0];

        dataCleared.push({ lon, val });
      }
      indexJump = index;
    }
  });
  if (flag) {
    dataCleared[0].lon += 1;
  }
  return dataCleared;
};

export const encodeMetaData = (objMetadata:Array<ImetaData>):string => {
  let stringEncode = '';
  objMetadata.forEach((val) => {
    stringEncode += String(val.lon) + val.val;
  });

  return stringEncode;
};

export const decodeString = (data: string):string => {
  const patronFilter = /^[0-9]+$/;
  const dataSplited = data.split('');
  let count = 0;
  let distanceAux = 0;
  let wordsDecoded = '';
  const arrayDecoded: any[] = [];
  if (dataSplited.length === 0) throw new Error('Empty file');
  dataSplited.forEach((val: string) => {
    if (!patronFilter.test(val)) {
      if (distanceAux === 0) {
        arrayDecoded.push(dataSplited.slice(distanceAux, count + 1));
      }
      if (distanceAux > 0) {
        arrayDecoded.push(dataSplited.slice(distanceAux + 1, count + 1));
      }

      distanceAux = count;
    }
    count += 1;
  });

  /*
      [
        [ '1', '2', 'W' ],
        [ '1', 'B' ],
        [ '1', '2', 'W' ],
        [ '3', 'B' ],
        [ '2', '4', 'W' ],
        [ '1', 'B' ],
        [ '1', '4', 'W' ]
      ]
    */
  arrayDecoded.forEach((val) => {
    wordsDecoded += val.pop().repeat(parseInt(val.join(''), 10));
  });

  return wordsDecoded;
};

export const testEncodeData = (dataToAnalyze:string):boolean => {
  const patronFilter = /^[0-9]+$/;
  const dataSplited = dataToAnalyze.split('');
  let flag = false;
  // eslint-disable-next-line consistent-return
  dataSplited.forEach((val, index) => {
    if (!patronFilter.test(val) && !patronFilter.test(dataSplited[index - 1])) {
      flag = true;
    }
  });
  return flag;
};
